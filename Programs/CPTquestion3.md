# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

*1. Describes the overall purpose of the program*

> The purpose of this project was to make a game where robots follow the player.

*2. Describes what functionality of the program is demonstrated in the
   video.*
>
> The functionality showed in the video was how the player can teleport, how the robots follow the player and how the robots crash into eachother.
>
*3. Describes the input and output of the program demonstrated in the
   video.*
>
> The input is when the person moves the player with the keys and the output is when the player moves, the robots crash and the player wins or loses.
>

## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

*1. The first program code segment must show how data have been
   stored in the list.*
>
> 
def place_robots(numbots):
    
    global robots

    robots = []

    while len(robots) < numbots:
        robot = Robot()
        robot.x = random.randint(0, 63)
        robot.y = random.randint(0, 47)
        robot.junk = False
        robot.shape = Box((10*robot.x, 10*robot.y), 10, 10)
        robots.append(robot)
>
2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.
>
> 
begin_graphics()
    
    game_over = False
    
    numbots = 4

    while not game_over:
        finished = False
        junk = []
        place_robots(numbots)
>

## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
>
> numbots
>
2. Describes what the data contained in the list represent in your
   program
>
> It represents the number of robots in the game.
>
3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list
>
> We would have to write and X and Y for every single one of the robots and that would make the code to long.
>

## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
>
>
    while key in ["KP_5", "s", "k"]:
        remove_from_screen(player.shape)
        safely_place_player()
        key = update_when("key_pressed")
>
>
>
2. The second program code segment must show where your student-developed
   procedure is being called in your program.
>
>
>
>

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
>
>
>
>
4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
>
>
>
>

## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.
>
> First call:
>
>
>
>
> Second call:
>
>
>
>
2. Describes what condition(s) is being tested by each call to the procedure
>
> Condition(s) tested by the first call:
>
>
>
>
> Condition(s) tested by the second call:
>
>
>
>
3. Identifies the result of each call
>
> Result of the first call:
>
>
>
>
> Result of the second call:
>
>
>
>
