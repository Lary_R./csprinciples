from gasp import *           # So that you can draw things
from random import randint   # Use this to get randint

class Player:
    pass

class Robot:
    pass

def place_player():
    global player
    
    player = Player()
    player.x = randint(0, 63)
    player.y = randint(0, 48)


def place_robots():
    global robots
    
    robots = []

    while len(robots) < numbots:
        robot = Robot()
        robot.x = randint(0, 63)
        robot.y = randint(0, 48)
        if not collided(robot, robots):
            robot.shape = Box(
                    (10 * robot.x , 10 * robot.y + 5), 10, 10, filled=False
                    )
            robots.append(robot)


def collided(player, robots):
    for robot in robots:
        if player.x == robot.x and player.y == robot.y:
            return True
    return False

def safely_place_player():
    global player
    place_player()

    while collided(player, robots):
        place_player()

    player.shape = Circle((10 * player.x + 5, 10 * player.y + 5), 5, filled=True)

def move_player():
    global player

    key = update_when('key_pressed')

    if key == 'd' and player.x < 63:
        player.x += 1
    elif key == 'c':
        if player.x < 63:
            player.x += 1
        if player.y > 0:
            player.y -= 1

    if key == 'w' and player.y < 47:
        player.y += 1
    elif key == 'q':
        if player.y < 47:
            player.y += 1
        if player.x > 0:
            player.x -= 1
    
    if key == 'a' and player.x > 0:
        player.x -= 1
    elif key == 'z':
        if player.x < 63:
            player.x -= 1
        if player.y > 0:
            player.y -= 1

    if key == 's' and player.y < 47:
        player.y -= 1
    elif key == 'e':
        if player.y < 47:
            player.y += 1
        if player.x > 0:
            player.x +=1

    while key == 'x':
        remove_from_screen(player.shape)
        safely_place_player()
        key = update_when('key_pressed')

    move_to(player.shape, (10 * player.x + 5, 10 * player.y + 5))

def move_robots():
    global robots
    global player
    for robot in robots:
        if robot.x > player.x:
            robot.x -= 1
        elif robot.x < player.x:
            robot.x += 1

        if robot.y > player.y:
            robot.y -= 1
        elif robot.y < player.y:
            robot.y += 1

        move_to(robot.shape, (10 * robot.x + 5, 10 * robot.y + 5))


def check_collision():
    global robots, player
    if collided(robots, player):
        Text("You’ve been caught!", (320, 240), size=48)
        return True
    else:
        return False


if __name__ == '__main__':
    begin_graphics()            # Create a graphics window
    numbots = 10
    finished = False
    place_robots()
    safely_place_player()

    
    while not finished:
        move_robots()
        move_player()
        finished = check_collision()

    sleep(3)
    end_graphics()  
