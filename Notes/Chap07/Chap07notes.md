# Repeating Steps
Learning Objectives:
- Use a for loop to repeat code.
- Use range to create a list of numbers.

A computer can do the same thigns over and over again without slipping up (as long as it has electricity). 

**How do we tell a computer to do things over and over again?**

We create a ***loop*** or ***itiration*** 

Ex. An Earworm. When a song is stuck in your head and it plays in a loop.


# Repeating with Numbers
We are going to use a ***for loop***. A ***for loop*** will use a variable and make the variable take on each of the values in a list of numbers one at a time. A list holds values in an order.

Notice that line 3 in the program below ends with a **:** and that line 4 is **indented** four spaces so that it starts under the n in number. Both the **:** and the **indention** are required in a loop. Line 3 is the start of the for loop and line 4 is the ***body*** of the loop. The ***body*** of the loop is repeated for each value in the list things_to_add.

What is the sum of all the numbers between 1 and 10? Run the program below to calculate the answer.

sum = 0  # Start out with nothing

things_to_add = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

for number in things_to_add
: sum = sum + number

print(sum)

*What is printed if you change the program above so that line 5 is also indented the same amount as line 4?*

It prints the value of sum 10 times and sum is different each time it is printed.

*What is printed if you change the program above so that lines 4 and 5 are not indented?*

You get an error


#What is a List?
A list holds items in order. A list in Python is enclosed in [ and ] and can have values separated by commas, like [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]. A list has an order and each list item has a position in the list, like the first item in a list or the last item in a list.

Once you change the program above in order to use * instead of +, you will see that it is still using the name (variable) sum to represent the product of all the numbers in things_to_add. The program would be better if we used the right name for the variable: product instead of sum once we switched to multiplication from addition. However, the program still works. In the end, the names for the variables are there for the benefit of the humans, not the computer. The computer doesn’t care if we name the variable xyzzy1776. It will work with a bad variable name. It’s just not as readable. **You should write your programs so that people can understand them, not just computers.**

# There’s a Pattern Here!
There’s a pattern in these programs, a pattern that is common when processing data. We call this the ***Accumulator Pattern***. In the first program above, we accumulated the values into the variable sum. In the last few programs, we accumulated a product into the variable product.

Here are the five steps in this pattern.

1. Set the accumulator variable to its initial value. This is the value we want if there is no data to be processed.
2. Get all the data to be processed.
3. Step through all the data using a for loop so that the variable takes on each value in the data.
4. Combine each piece of the data into the accumulator.
5. Do something with the result.

# Adding Print Statements

The goal of this stage of learning about programming is to develop a mental model of how the program works. Can you look at a program and predict what’s going to happen? Can you figure out the values of the variables? Feel free to insert lots of print() function calls. Make a prediction about variable values, then insert print() calls to display the variable values, and run the program to find out whether the prediction is right.

# Vocab
- Accumulator Pattern - The accumulator pattern is a set of steps that processes a list of values. One example of an accumulator pattern is the code to sum a list of numbers.

- Body of a Loop - The body of a loop is a statement or set of statements to be repeated in a loop. Python uses indention to indicate the body of a loop.

- Indention - Indention means that the text on the line has spaces at the beginning of the line so that the text doesn’t start right at the left boundary of the line. In Python indention is used to specify which statements are in the same block. For example the body of a loop is indented 4 more spaces than the statement starting the loop.

- teration - Iteration is the ability to repeat a step or set of steps in a computer program. This is also called looping.

- List - A list holds a sequence of items in order. An example of a list in Python is [1, 2, 3].

- Loop - A loop tells the computer to repeat a statement or set of statements.

# Summary of Python Keywords and Functions

- def - The def keyword is used to define a procedure or function in Python. The line must also end with a : and the body of the procedure or function must be indented 4 spaces.

- for - A for loop is a programming statement that tells the computer to repeat a statement or a set of statements. It is one type of loop.

- print - The print statement in Python will print the value of the items passed to it.

- range - The range function in Python returns a list of consecutive values. If the range function is passed one value it returns a list with the numbers from 0 up to and not including the passed number. For example, range(5) returns a list of [0, 1, 2, 3, 4]. If the range function is passed two numbers separated by a comma it returns a list including the first number and then up to but not including the second number. For example, range(1, 4) returns the list [1, 2, 3]. If it is passed three values range(start,end,step) it returns all the numbers from start to one less than end changing by step. For example, range(0, 10, 2) returns [0, 2, 4, 6, 8].

