product = 1      # Start with the multiplicative identity
numbers = [1, 2, 3, 4, 5]
for num in numbers:
    product = product * num
 
print(product)     # Print the result
