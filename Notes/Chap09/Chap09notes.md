# Using Repetition with Strings
Learning Objectives:

- Show how the accumulator pattern works for strings.
- Show how to reverse a string.
- Show how to mirror a string.
- Show how to use a while loop to modify a string.

Python already has built in the ability to play with words or **strings**, just like how we played with numbers in the last chapter. A **string** is a collection of letters, *digits*, and other characters. A Python ***for*** loop knows how to step through letters, and addition (+) appends strings together. What’s cool is that the same accumulator pattern works.

As a reminder, here are the five steps in the accumulator pattern.

1. Set the accumulator variable to its initial value. This is the value we want if there is no data to be processed.
2. Get all the data to be processed.
3. Step through all the data using a ***for*** loop so that the variable takes on each value in the data.
4. Combine each piece of the data into the accumulator.
5. Do something with the result.

Run this next one, and look at how a simple change to the pattern gives a very different result. Here we’ll combine before rather than afterward, changing only Step 4 (how values are accumulated).

**Step 1: Initialize accumulators**

new_string_a = ""

new_string_b = ""

**Step 2: Get data**

phrase = "Happy Birthday!"

**Step 3: Loop through the data**

for letter in phrase:

    # Step 4: Accumulate

    new_string_a = letter + new_string_a

    new_string_b = new_string_b + letter

**Step 5: Process result**

print("Here's the result of using letter + new_string_a:")

print(new_string_a)

print("Here's the result of using new_string_b + letter:")

print(new_string_b)

# Modifying Text

We can loop through the string and modify it using find and slice (substring).
Google has been digitizing books. But, sometimes the digitizer makes a mistake and uses 1 for i. 

1. a_str = "Th1s is a str1ng"
2. pos = a_str.find("1")
3. while pos >= 0:
4.	 a_str = a_str[0:pos] + "i" + a_str[pos+1:len(a_str)]
5.	 pos = a_str.find("1")
6. print(a_str)

*You don’t have to replace just one character. You could replace every instance of a word with another word.*

# Vocab

- Accumulator Pattern - The accumulator pattern is a set of steps that processes a list of values. One example of an accumulator pattern is the code to reverse the characters in a string.

- Palindrome - A palindrome has the same letters if you read it from left to right as it does if you read it from right to left. An example is "A but tuba".

- String - A string is a collection of letters, numbers, and other characters like spaces inside of a pair of single or double quotes.

- def - The def keyword is used to define a procedure or function in Python. The line must also end with a : and the body of the procedure or function must be indented 4 spaces.

- for - A for loop is a programming statement that tells the computer to repeat a statement or a set of statements. It is one type of loop.
print - The print statement in Python will print the value of the items passed to it.

- range - The range function in Python returns a list of consecutive values. If the range function is passed one value it returns a list with the numbers from 0 up to and not including the passed number. For example, range(5) returns a list of [0, 1, 2, 3, 4]. If the range function is passed two numbers separated by a comma it returns a list including the first number and then up to but not including the second number. For example, range(1, 4) returns the list [1, 2, 3]. If it is passed three values range(start, stop, step) it returns all the numbers from start to one less than end changing by step. For example, range(0, 10, 2) returns [0, 2, 4, 6, 8].

- while - A while loop is a programming statement that tells the computer to repeat a statement or a set of statements. It repeats the body of the loop while a logical expression is true.
