miles_per_hour = 70
miles_travelled = 140
hours_taken = miles_travelled / miles_per_hour
print(f"A car travelling at {miles_per_hour} mph takes {hours_taken} hours to go {miles_travelled} miles.")


