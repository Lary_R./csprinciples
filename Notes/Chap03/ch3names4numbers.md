# CS Chapter 3 Notes

##Assigning a Name

Learning Objectives:

-Understand the concept of a variable.

-Assign a value to a variable.

-Use assignment in calculations.

-Understand the ways that students get assignments wrong.

-Reuse variables across different assignment statements.

In programming languages, setting a variable’s value is also called **assignment**. *EX.* A statement like a = 4 means that the symbol a refers to space (in the computer’s memory) that is assigned the value 4.

###Legal Variable Names

There are restrictions on what you can use as a variable name.

1. It must start with a letter (uppercase like A or lowercase like a) or an underscore _

2. It can also contain digits, like 1 or 9, just not as the first character.

3. It can’t be a Python keyword such as and, def, elif, else, for, if, import, in, not, or, return, or while. These have special meaning in Python and are part of the language.

4. Case does matter. A variable named result is not the same as one named Result.

Since you can’t have spaces in a variable name you can either join words together by uppercasing the first letter of each new word like heightInInches or use underscores between words height_in_inches. Uppercasing the first letter of each new word is called camel-case or mixed-case.


#Expressions

The right hand side of the assignment statement doesn’t have to be a value. It can be an arithmetic expression. *EX.* 2x2

the **modulo** (remainder) operator %
: It returns the remainder when you divide the first number by the second.

 *The result of x % y when x is smaller than y is always x*

The order that expressions are executed is the same as it is in math and is shown in the table below from highest precedence to lowest. If two symbols have the same precedence they are evaluated from left to right.

Expression	Arithmetic meaning

-1 + 2	Addition, the result is 3

-3 * 4	Multiplication, the result is 12

-1 / 3	Integer division, the result is 0 in older Python environments, but 0.333333333333 in Python 3

-2.0 / 4.0	Division, the result is 0.5, since you are using decimal numbers in the calculation

-1 // 3	Floor division, the result is 0 in Python 3

-2 % 3	Modulo (remainder), the result is 2

-(-1)	Negation, the result is -1

/Users/1024074/Projects/csprinciples/Resources/PictureforCS.jpg

/Users/1024074/Projects/csprinciples/Resources/PictureforCS2.jpg 

What we’re doing above is tracing a program.

We run a program – tell the computer to execute each step of the program as fast as possible.

The function print can take an input (a variable name inside of parentheses) whose value will be displayed. (The print function can also print a string (like "Cost to get from Chicago to Dallas") which is a sequence of characters inside a pair of double quotes as seen in line 6. It will print the exact contents of the string.)

The sequence of statements in a program is very important. Assignment doesn’t create some kind of relationship between the names, like in mathematics. The variable a might equal 12 at one point, and 15 at another. An assignment statement is an action that occurs once, and then is over with.

We can see values (including the values for named variables) by printing them. It’s a useful way to see what’s going on inside a program.

We can use variables to solve problems like those we might solve in a spreadsheet.

We don’t really have to create new variables quantity2 and unit_price2. We only use those to compute the total for the line, and then we could reuse those variable names.

/Users/1024074/Downloads/PictureforCS3.jpg 

**It is best to use variable names that make sense.**

Arithmetic Expression 
: An arithmetic expression contains a mathematical operator like - for subtraction or * for multiplication.

Assignment 
: Assignment means setting a variable’s value. For example, x = 5 assigns the value of 5 to a variable called x.

Assignment Dyslexia
: Assignment Dyslexia is putting the variable name on the right and the value on the left as in 5 = x. This is not a legal statement.

Integer Division
: Integer division is when you divide one integer by another. In some languages this will only give you an integer result, but in Python 3 it returns a decimal value, unless you use the floor division operator, //.

Tracing
: Tracing a program means keeping track of the variables in the program and how their values change as the statements are executed. We used a Code Lens tool to do this in this chapter.

