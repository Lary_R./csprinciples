def triangleDraw(n):
    x = 1
    while x <= n:
        print("*" * x)
        x += 1

triangleDraw(6)
