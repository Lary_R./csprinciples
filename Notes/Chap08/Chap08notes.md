# Loops - For and While

Learning Objectives:

- Introduce the concept of a while loop, which will repeat the body of a loop while a logical expression is true.
- Introduce the concept of a logical expression, which is either true or false.
- Introduce the concept of an infinite loop, which is a loop that never ends.
- Compare and contrast while and for loops

We have been using a *for* loop to repeat the **body of a loop** a known number of times. The **body of a loop** is all the statements that follow the *for* statement and are indented to the right of it. Be sure to indent 4 spaces to indicate the statements that are part of the body of the loop.

# Introducing the While Loop

Another way to repeat statements is the *while* loop. It will repeat the **body of loop** as long as a **logical expression** is true. The **body of the loop** is the statement or statements that are indented 4 or more spaces to the right of the *while statement*. A **logical expression** is one that is either true or false, like x > 3.

*While* loops are typically used when you don’t know how many times to execute the loop. For example, when you play a game you will repeat the game while there isn’t a winner. If you ever played musical chairs you walked around the circle of chairs while the music was playing.

# Infinite Loops

Getting a computer to repeat a set of statements is simple. Sometimes it can be tricky to get it to stop. Remember that a while loop will execute as long as the logical expression is true. What happens if the logical expression is always true?

So, here’s a program that loops forever.

while 1 == 1:
    print("Looping")
    print("Forever")

Since *1* will always be equal to *1*, the two *print* statements will just be repeated over and over and over again and the logical expression will never be false. We call that an **infinite loop**, which means a loop that continues forever or until it is forced to stop.

We ran the following code in a form of Python where we could stop the computer easily:

>>> while 1 == 1:
...     print("Looping")
...     print("Forever")
...
Looping
Forever
Looping
Forever
Looping
Forever
Looping
Forever

**Ex.**
It’s easy to have the computer repeat something a specific number of times. We have done this with a for loop and a list of numbers created with the range function as shown below.

1. for counter in range(1,11):
2. 	 print(counter)

We can also do it with a while loop.

For example, we could have a computer count up from 1 to 10. We will use a counter variable that we will increment inside the loop. Increment means increase the value by one. Note that we continue the loop as long as the counter is less than the desired last value plus one.

1. counter = 1
2.	while counter < 11:
3.	    print(counter)
4.	    counter = counter + 1

# Side by Side Comparison of a For Loop and a While Loop

Let’s look at these loops side by side. The first line in the *for* loop creates the variable *counter* and the list of values from 1 to 10. It then sets *counter* equal to 1 and executes the body of the loop. In the body of the loop it prints the current value of *counter* and then changes *counter* to the next value in the list.

The first line of the *while* loop creates the variable *counter* and sets its value to 1. The second line tests if the value of *counter* is less than 11 and if so it executes the body of the loop. The body of the loop prints the current value of *counter* and then increments the value of *counter*. The loop will stop repeating when *counter* is equal to 11.

a for loop next to an equivalent while loop

Which is the best loop to use when you want to execute a loop a known number of times? Which way uses less code or seems less error prone? The problem with using a *while* loop to repeat code a specific number of times is that you may forget to change the value that you are testing inside the body of the loop and in that case you will have an infinite loop.

The following code is an attempt to show another way to print the values from 1 to 10. **However, it currently has an error and is an infinite loop.** Fix the code below so that it isn’t an infinite loop.

# Looping When We Don’t Know When We’ll Stop

While loops are typically used when you don’t know how many times the loop needs to repeat. The body of the loop will repeat while the condition is true. The logical expression will be evaluated just before the body of the loop is repeated.

Let’s say that we want to find the square root of a number. For some square roots, you’re never going to be exact. Let’s say that we want to find a square root that, when multiplied by itself, is within 0.01 of the square we want. How do we do it? There’s a really old process that we can apply here.

1. Start by guessing 2.
2. Compute the guess squared.
3. Is the guess squared close to the target number? If it’s within 0.01, we’re done. We’ll take the absolute value of the difference, in case we overshoot. (In Python, *abs* is the absolute value function.)
4. If it’s not close enough, we divide the target number by our guess, then average that value with our guess.
5. That’s our new guess. Square it, and go back to Step 3.

# Nested For Loops

The body of any loop, can even include…another loop! Here is a super-simple program that generates all the times tables from 0 to 10. The str() function changes a numeric value into a string.

1. for x in range(0, 11):
2.	    for y in range(0, 11):
3.	        print(str(x) + " * " + str(y) + " = " + str(x*y))

# Chapter 8 - Summary

- Body of a Loop - The body of a loop is a statement or set of statements to be repeated in a loop. The body of a loop is indicated in Python with indention.
- Counter - A counter is a variable that is used to count something in a program.
- Increment - Increment means to increase the value of a variable by 1.
- Infinite Loop - An infinite loop is one that never ends.
- Logical Expression - An logical expression is either true or false. An example of a logical expression is ***x < 3***.

# Summary of Python Keywords and Functions

- def - The def keyword is used to define a procedure or function in Python. The line must also end with a : and the body of the procedure or function must be indented 4 spaces.
- for - A for loop is a programming statement that tells the computer to repeat a statement or a set of statements. It is one type of loop.
- print - The print statement in Python will print the value of the items passed to it.
- range - The range function in Python returns a list of consecutive values. If the range function is passed one value it returns a list with the numbers from 0 up to and not including the passed number. For example, range(5) returns a list of [0, 1, 2, 3, 4]. If the range function is passed two numbers separated by a comma it returns a list including the first number and then up to but not including the second number. For example, range(1, 4) returns the list [1, 2, 3]. If it is passed three values range(start, stop, step) it returns all the numbers from start to one less than stop changing by step. For example, range(0, 10, 2) returns [0, 2, 4, 6, 8].
- while - A while loop is a programming statement that tells the computer to repeat a statement or a set of statements. It repeats the body of the loop while a logical expression is true.

