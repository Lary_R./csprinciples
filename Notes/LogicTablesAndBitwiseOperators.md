Logic Tables:

- Logic tables are often used to determine the output of a logical expression for a given set of input values.

- Logic tables can be used to evaluate logical operators such as AND, OR, NOT, XOR, and NAND.

- In a logic table, the output column represents the result of the logical expression for each set of input values, and the input columns represent the individual inputs to the expression.
 
- Logic tables can also be used to determine the truth or falsehood of a complex logical statement that involves multiple logical operators and inputs.


Bitwise Operators:

- Bitwise operators are used to manipulate the individual bits of a number.

- The bitwise AND operator (&) sets each bit in the result to 1 only if both corresponding bits in the operands are 1.

- The bitwise OR operator (|) sets each bit in the result to 1 if either corresponding bit in the operands is 1.

- The bitwise XOR operator (^) sets each bit in the result to 1 if the corresponding bits in the operands are different.

- The bitwise NOT operator (~) flips all the bits in the operand, changing 0s to 1s and vice versa.

- The left shift operator (<<) shifts the bits in the operand to the left by a specified number of positions, adding zeros to the right.

- The right shift operator (>>) shifts the bits in the operand to the right by a specified number of positions, filling in the leftmost bits with copies of the original leftmost bit (which preserves the sign of the operand if it is a signed number).

- Bitwise operators are often used in low-level programming tasks, such as working with hardware or manipulating data structures that store data at the bit level.
