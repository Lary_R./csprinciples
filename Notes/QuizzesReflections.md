# Quiz 1

## Question 3

### The figure below shows a circuit composed of two logic gates. The outp of the ciruit is true.

- [x] A. Input A must be True
- [ ] B. Input A must be False
- [ ] C. Input A can eithe rbe true or false
- [ ] D. there is no possible value of input A that will cause the circuit to output true.

- My first answer was C because I did not know how OR and AND statemnts worked.

- The real answer is A because in OR statements one or both have to be true. In AND statements BOTH have to be true so the input could onyl be true if we ended up with a true output.

## Question 4

### An Automobile comapmny uses GPS technology tom implement a smartphone app for its custumers taht they can use to call their self-driving electrics cars to their location. When the app is used, the coordinates of the car owner are sent to their car. The car then proceeds to drive itself from parking lots to owner. WWhat is possible concer for user privacy when using the app?

- [ ] A. Idling period for which the car is running in anticipation of the signal would cause excess fuel consumption.
- [ ] B. Unauthorized access to vehicles through the app.
- [x] C. User location data could be used by companies and third-party stakeholders.
- [ ] D. Interference of signals if two people use the app simultaneously and close by

- My first answer was D because i did not read the question correctly and I thought they meant any issue that would be caused.

- The real answer is C because the question was about what concersn there were for user PRIVACY.

## Question 5

### A programmer has developed an algorithm that computes the sum of integers taken from a list. However, the programmer wants to modify the cod sum up the even numbers from the list. WHat programming structure can the programmer add to the list?

- [ ] A. Sequencing
- [ ] B. Iteration
- [ ] C. Searching
- [x] D. Selection

- My first answer was Sequencing because It was the only familiar word that made sence to me sicne I thought that Selection was just picking things at random.

- The real answer was D because selection is picking something and trying it.

## Question 7

### Every night at 12:00am you notice that some files are being uploaded form your PC device to an unfamiliar website without your knowledge. You check the file and find that these files are from the days work at the office. What is this an exmaple of?

- [ ] A. Phising attack
- [ ] B. Keylogger attack
- [x] C. Computer Virus
- [ ] D. Malware insertion point

- My first answer was B because I did not know that computer viruses could download your data and malware insertion point did not make sense.I did not even consider phising because I already knew the definiton of that.

- The real answer is Computer virus because a keylogger attack is when someone else downloads software onto your device and a computer virus is a program that was downloading the data.

## Question 9

### Musician record their songs on a computer. When they listen to a digitally saved copy of their song, they find the sound quality lower tha expected. Which of the following could be possible reason why this difference exist?

- [ ] A. The file was moved from the original location, cauisng some information to be lost. 
- [ ] B. The recording was done through the lossless compression technique
- [ ] C. The song file was saved with higher bits per second
- [x] D. The song file was saved with lower bits per second

- My first answer was B because I thought the  recording would lose quality after going though that compression.

- The real answer is D because lossless compression wouldnt make it have less quality but having lower bits per second would make the quality worse.

## Question 10

### An organization needs to process large amounts of data that cannot be handled by the current PC setup and has extra PC devices lying around in the storage room. What sort of connection shoudl this organization consider to improve its processing capabilities.


- [ ] A. The organization should consider maintaining their sequential computing model.
- [x] B. The organization should consider using a distibution computing model
- [ ] C. The organization should consider limiting the data collection requirements.
- [ ] D. The organization should consider adding additional servers

- My first answer was A because I guessed sicne I didnt undertsand the question or the answers.

- The real answer was B because the organization needed to improve their processing capabilities and using a distributed computing model becaude its taking a big load and distributing it.

# Quiz 4

## Question 1

### What can replace the missing condition to complete the last column of the table?

- [ ] A. A and B
- [ ] B. A or B
- [ ] C. Not(A or B)
- [x] D. A and NOT(B)

- My first answer was A because I didnt know what I was suppose to be finding and how the column worked.

- The real answer is D because AND NOT turns the B into the opposite so if its TRUE then it turns into FALSE. AND needs for both to be TRUE.

## Question 3

### A team of researchers wants to create a program to anylyze the moaunt of pollution reported in roughly 3,00 counties across the United States. The program is intended to combine coundty data sets and the process the data. Which of the following is most likely to be a challenge in creating the program?

- [ ] A. A computer cannot combine data from different files.
- [x] B. Different counties may organize data in different ways.
- [ ] C. The  number of counties is too large for the program to process
- [ ] D. The total number of rows of data is too large for the program to process.

- My first answer was C becasue I did not know you could download a bigger processor

- The real answer is B because all the other answers have solutions.

## Question 5

### A database of information about shows at a concert venue conatins the folowing information. 
	- Name of artist performing at the show
	- Date of show
	- Total dollar amount of all tickest sold
### Which of the folowing addtional peices of information in fingidng the artist with the greatest attendance.

- [x] A. Average Ticket Price
- [ ] B. Length of the show in minutes
- [ ] C. Start time of the show
- [ ] D. Total amount of food and drinks sold during the show.

- My first answer was D becasue I thought that would help us figure out how many people bought food during show. 

- The real answer is A becasue you have the toal amount of dollar from all tickest sold if we also have the average price for the ticket we can use that to firgure out how many people went.

## Question 8

### Consider the folowing procedure called mystery, which is inteded to display the number of times a number target appears in the list. Which of the following best describes the behaiveur of the procedure? 2 answers

- [x] A. The program correctly displays the count if the target is not in the list
- [ ] B. The program never correctly displays th correct value for count.
- [x] C. The program correctly dusplays the count uf the target appears once in the list and at the end of the list.
- [ ] D. The program always correctly displays the correct value for count.

- My first answer was B and D because B was the only one that made sense to me and I guessed D.

- The real answer was A and D because if its never in the list it will always be zero.

# Quiz 2

## Question 6

### If the current red value is 1011101, what would be the new value in binary if the red calue is increased by 4 in decimal?

- [ ] A. 157bin
- [ ] B. 0100bin
- [x] C. 10011111bin
- [ ] D. 10100001bin

- My first answer was D because I didnt do the math correctly

- The real answer was C because I redid the math

## Question 9

### Using a binary search, howm many iteration would it take to find the letter W?

- [ ] A. 2
- [x] B. 3
- [ ] C. 23
- [ ] D. 24

- My first answer was 23 becasue I did not know what a Binary search was

- The real answer was B because we divide the string by 2 until we get to W.

# Quiz 3

## Question 3

### Digital alarm clock display informationa nd visual indicators to help wake people up on time. Which of these indicators coudl represent a single bit?

- [ ] A. The current month (1-12)
- [x] B. PM/AM
- [ ] C. The current hour (1-12)
- [x] D. The temperature indicator C or F 

- My first answer was B and C because I though the answers had to match.

- The real answer was B and D because A single bit only gives you 2 options.

## Question 6

### The same task is performed through sequential and parallel computing models. In the taken by the sequential setup was 30 seconds, while it took the prallel model only 10 seconds. What will be the speed up parameter for working with the parallel model form now on?

- [x] A. 0.333
- [ ] B. 3
- [ ] C. 300
- [ ] D. 20

- My first answer was B because i was just guessing.

- The real answer was A becasue the faster it goes the smaller the number.

