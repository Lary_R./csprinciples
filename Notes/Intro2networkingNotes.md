# Chap 1

## Vocab

- address: A number that is assigned to a computer so that messages can be routed to the computer.

- hop: A single physical network connection. A packet on the Internet will typically make several “hops” to get from its source
computer to its destination.

- LAN: Local Area Network. A network covering an area that is
limited by the ability for an organization to run wires or the power
of a radio transmitter.

- leased line: An “always up” connection that an organization
leased from a telephone company or other utility to send data
across longer distances.

- operator (telephone): A person who works for a telephone company and helps people make telephone calls.

- packet: A limited-size fragment of a large message. Large messages or files are split into many packets and sent across the
Internet. The typical maximum packet size is between 1000 and
3000 characters.

- router: A specialized computer that is designed to receive incoming packets on many links and quickly forward the packets on the
best outbound link to speed the packet to its destination.

- store-and-forward network: A network where data is sent
from one computer to another with the message being stored
for relatively long periods of time in an intermediate computer
waiting for an outbound network connection to become available.

- WAN: Wide Area Network. A network that covers longer distances, up to sending data completely around the world. A WAN
is generally constructed using communication links owned and
managed by a number of different organizations.

## Test

1. What did early telephone operators do?

Maintained cell phone towers

* Connected pairs of wires to allow people to talk *

Installed copper wire between cities

Sorted packets as they went to the correct destination

2. What is a leased line?

A boundary between leased and owned telephone equipment

A connection between a keyboard and monitor

A wire that ran from one phone company office to another

* An "always on" telephone connection *

3. How long might a message be stored in an intermediate computer for a store-and-forward network?

less than a second

no more than four seconds

less than a minute

* possibly as long as several hours *

4. What is a packet?

A technique for wrapping items for shipping

A small box used for storage

* A portion of a larger message that is sent across a network *

The amount of data that could be stored on an early punched card

5. Which of these is most like a router?

* A mail sorting facility *

A refrigerator

A high-speed train

An undersea telecommunications cable

6. What was the name given to early network routers?

Interfaith Message Processors

Internet Motion Perceptrons

Instant Message Programs

* Interface Message Processors *

7. In addition to breaking large messages into smaller segments to be sent, what else was needed to properly route each message segment?

* A source and destination address on each message segment *

An ID and password for each message segment

A small battery to maintain the storage for each message segment

A small tracking unit like a GPS to find lost messages

8. Why is it virtually free to send messages around the world using the Internet?

Because governments pay for all the connections

Because advertising pays for all the connections

* Because so many people share all the resources *

Because it is illegal to charge for long-distance connections

# Chap 2

## Vocab

- client: In a networked application, the client application is the
one that requests services or initiates connections.

- fiber optic: A data transmission technology that encodes data
using light and sends the light down a very long strand of thin
glass or plastic. Fiber optic connections are fast and can cover
very long distances.

- offset: The relative position of a packet within an overall message or stream of data.

- server: In a networked application, the server application is the
one that responds to requests for services or waits for incoming
connections.

- window size: The amount of data that the sending computer is
allowed to send before waiting for an acknowledgement

## Test

1. Why do engineers use a “model” to organize their approach
to solving a large and complex problem?

a) Because it allows them to build something small and test it
in a wind tunnel

b) Because talking about a model delays the actual start of the
hard work

* c) Because they can break a problem down into a set of smaller
problems that can be solved independently *

d) Because it helps in developing marketing materials

2. Which is the top layer of the network model used by TCP/IP
networks?

* a) Application *

b) Transport

c) Internetwork

d) Link

3. Which of the layers concerns itself with getting a packet of
data across a single physical connection?

a) Application

b) Transport

c) Internetwork

* d) Link *

4. What does CSMA/CD stand for?

* a) Carrier Sense Multiple Access with Collision Detection *

b) Collision Sense Media Access with Continuous Direction

c) Correlated Space Media Allocation with Constant Division

d) Constant State Multiple Address Channel Divison

5. What is the goal of the Internetwork layer?

a) Insure that no data is lost while enroute

* b) Get a packet of data moved across multiple networks from
its source to its destination *

c) Make sure that only logged-in users can use the Internet

d) Insure than WiFi is fairly shared across multiple computers

6. In addition to the data, source, and destination addresses,
what else is needed to make sure that a message can be
reassembled when it reaches its destination?

* a) An offset of where the packet belongs relative to the beginning of the message *

b) A location to send the data to if the destination computer is
down

c) A compressed and uncompressed version of the data in the
packet

d) The GPS coordinates of the destination computer

7. What is “window size”?

a) The sum of the length and width of a packet

b) The maximum size of a single packet

c) The maximum number of packets that can make up a message

* d) The maximum amount of data a computer can send before
receiving an acknowledgement *

8. In a typical networked client/server application, where does
the client application run?

* a) On your laptop, desktop, or mobile computer *

b) On a wireless access point

c) On the closest router

d) In an undersea fiber optic cable

9. What does URL stand for?

a) Universal Routing Linkage

b) Uniform Retransmission Logic

* c) Uniform Resource Locator *

d) Unified Recovery List

# Chap 3

The Link layer is the lowest layer in the four-layer architecture, responsible for moving data across a single "hop." Engineers designing and building Link layer technologies can focus solely on issues related to the Link layer, such as connection distance, voltage, frequency, and speed, without worrying about the layers above. This allows them to create the best possible solution for data transmission. Today's Link layer technologies like WiFi, satellite, cable modems, Ethernet, and cellular networks are very well developed, enabling seamless and fast data transmission without much user intervention.

## Vocab

- base station: Another word for the first router that handles your
packets as they are forwarded to the Internet.

- broadcast: Sending a packet in a way that all the stations connected to a local area network will receive the packet.

- gateway: A router that connects a local area network to a wider
area network such as the Internet. Computers that want to send
data outside the local network must send their packets to the
gateway for forwarding.

- MAC Address: An address that is assigned to a piece of network
hardware when the device is manufactured.

- token: A technique to allow many computers to share the same
physical media without collisions. Each computer must wait until
it has received the token before it can send data

## Test

1. When using a WiFi network to talk to the Internet, where
does your computer send its packets?

**a) A gateway**

b) A satellite

c) A cell phone tower 

d) The Internet Central Office

2. How is the link/physical address for a network device assigned?

a) By the cell tower

b) By the Internet Assignment Numbers Authority (IANA)

**c) By the manufacturer of the link equipment**

d) By the government

3. Which of these is a link address?

**a) 0f:2a:b3:1f:b3:1a**

b) 192.168.3.14

c) www.khanacademy.com

d) @drchuck

4. How does your computer find the gateway on a WiFi network?

a) It has a gateway address installed by the manufacturer

**b) It broadcasts a request for the address of the gateway**

c) It repeatedly sends a message to all possible gateway addresses until it finds one that works

d) The user must enter the gateway address by hand

5. When your computer wants to send data across WiFi, what
is the first thing it must do?

**a) Listen to see if other computers are sending data**

b) Just start sending the data

c) Send a message to the gateway asking for permission to
transmit

d) Wait until informed that it is your turn to transmit

6. What does a WiFi-connected workstation do when it tries to
send data and senses a collision has happened?

a) Keep sending the message so part of the message makes it
through

b) Wait until told by the gateway that the collision is over

c) Immediately restart transmitting the message at the beginning

**d) Stop transmitting and wait a random amount of time before
restarting**

7. When a station wants to send data across a “token”-style
network, what is the first thing it must do?

a) Listen to see if other computers are sending data

b) Just start sending the data

c) Send a message to the gateway asking for permission to
transmit

**d) Wait until informed that it is your turn to transmit**

### 0f:2a:b3:1f:b3:1a = 101101001011011

# Chap 5

Domain Name System (DNS) is a critical part of the Internet infrastructure that allows users to use symbolic names instead of numeric IP addresses to connect to servers. By providing a service that maps domain names to IP addresses, it enables servers to be moved from one Internet connection to another without requiring users to manually change their configurations.

To purchase a domain name, there are many domain name registrars to choose from.

## Vocab

- DNS: Domain Name System. A system of protocols and servers
that allow networked applications to look up domain names and
retrieve the corresponding IP address for the domain name.

- domain name: A name that is assigned within a top-level domain. For example, khanacademy.org is a domain that is assigned
within the “.org” top-level domain.

- ICANN: International Corporation for Assigned Network Names
and Numbers. Assigns and manages the top-level domains for
the Internet.

- registrar: A company that can register, sell, and host domain
names.

- subdomain: A name that is created “below” a domain
name. For example, “umich.edu” is a domain name and
both “www.umich.edu” and “mail.umich.edu” are subdomains
within “umich.edu”.

- TLD: Top Level Domain. The rightmost portion of the domain
name. Example TLDs include “.com”, “.org”, and “.ru”. Recently,
new top-level domains like “.club” and “.help” were added.

## Test

1. What does the Domain Name System accomplish?

**a) It allows network-connected computers to use a textual
name for a computer and look up its IP address**

b) It keeps track of the GPS coordinates of all servers

c) It allows Regional Internet Registries (RIRs) to manage IP addresses on the various continents

d) It assigns different IP addresses to portable computers as
they move from one WiFi to another

2. What organization assigns top-level domains like “.com”,
“.org”, and “.club”?

a) IANA - Internet Assigned Numbers Authority

b) IETF - Internet Engineering Task Force

**c) ICANN - International Corporation for Assigned Network
Names and Numbers**

d) IMAP - Internet Mapping Authorization Protocol

3. Which of these is a domain address?

a) 0f:2a:b3:1f:b3:1a

b) 192.168.3.14

**c) www.khanacademy.org**

d) @drchuck

4. Which of these is not something a domain owner can do with
their domain?

a) Create subdomains

b) Sell subdomains

**c) Create new top-level domains**

d) Assign an IP address to the domain or subdomain

# Chap 6

In a sense, the purpose of the Transport layer is to compensate
for the fact that the Link and Internetworking layers might lose
data. When the two lower layers lose or reroute packets, the
Transport layer works to reassemble and/or retransmit that data.
The existence of the Transport layer makes it possible for the two
lower layers to ignore retransmission and rate-limiting issues.
Part of the goal of a layered architecture is to break an overly
complex problem into smaller subproblems. Each layer focuses
on solving part of the overall problem and assumes that the other
layers solve the problems they are supposed to solve.

## Vocab

- acknowledgement: When the receiving computer sends a notification back to the source computer indicating that data has
been received.

- buffering: Temporarily holding on to data that has been sent or
received until the computer is sure the data is no longer needed.

- listen: When a server application is started and ready to accept
incoming connections from client applications.

- port: A way to allow many different server applications to be
waiting for incoming connections on a single computer. Each
application listens on a different port. Client applications make
connections to well-known port numbers to make sure they are
talking to the correct server application.

## Test

1. What is the primary problem the Transport (TCP) layer is supposed to solve?

a) Move packets across multiple hops from a source to destination computer

b) Move packets across a single physical connection

c) Deal with lost and out-of-order packets

**d) Deal with encryption of sensitive data**

2. What is in the TCP header?

a) Physical address

b) IP Address and Time to Live

**c) Port number and offset**

d) Which document is being requested

3. Why is “window size” important for the proper functioning of
the network?

a) Because packets that are too large will clog fiber optic connections

**b) It prevents a fast computer from sending too much data on
a slow connection**

c) It limits the number of hops a packet can take before it is
dropped

d) It determines what part of an IP address is the network number

4. What happens when a sending computer receives an acknowledgement from the receiving computer?

a) The sending computer resends the data to make sure it was
transmitted accurately

**b) The sending computer sends more data up to the window
size**

c) The sending computer sends an “acknowledgment for the
acknowledgment”

d) The sending computer sends the acknowledgement to the
Internet Map (IMAP)

5. Which of these detects and takes action when packets are
lost?

a) Sending computer

b) Network gateway

c) Core Internet routers

**d) Receiving computer**

6. Which of these retains data packets so they can be retransmitted if a packets lost?

**a) Sending computer**

b) Network gateway

c) Core Internet routers

d) Receiving computer

7. Which of these is most similar to a TCP port?

a) Train station

b) Undersea network cable

**c) Apartment number**

d) Sculpture garden

8. Which half of the client/server application must start first?

**a) Client**

b) Server

9. What is the port number for the Domain Name System?

a) 22

b) 80

**c) 53**

d) 143

10. What is the port number for the IMAP mail retrieval protocol?

a) 22

b) 80

c) 53

**d) 143**

# Chap 7

The purpose of the lower three layers of the network model (Transport, Internetwork, and Link) is to handle the complexity of moving data across a network, allowing applications in the Application layer to focus on solving the application problem. This simplicity has led to a wide range of networked applications, and it is now easy to experiment and build new types of networked applications to solve problems that have not yet been imagined.

## Vocab

- HTML: HyperText Markup Language. A textual format that
marks up text using tags surrounded by less-than and
greater-than characters. Example HTML looks like: <p>This
is <strong>nice</strong></p>.

- HTTP: HyperText Transport Protocol. An Application layer protocol that allows web browsers to retrieve web documents from web
servers.

- IMAP: Internet Message Access Protocol. A protocol that allows
mail clients to log into and retrieve mail from IMAP-enabled mail
servers.

- flow control: When a sending computer slows down to make
sure that it does not overwhelm either the network or the destination computer. Flow control also causes the sending computer
to increase the speed at which data is sent when it is sure that
the network and destination computer can handle the faster data
rates.

- socket: A software library available in many programming languages that makes creating a network connection and exchanging data nearly as easy as opening and reading a file on your
computer.

- status code: One aspect of the HTTP protocol that indicates
the overall success or failure of a request for a document. The
most well-known HTTP status code is “404”, which is how an HTTP
server tells an HTTP client (i.e., a browser) that it the requested
document could not be found.

- telnet: A simple client application that makes TCP connections
to various address/port combinations and allows typed data to be
sent across the connection. In the early days of the Internet, telnet was used to remotely log in to a computer across the network.

- web browser: A client application that you run on your computer
to retrieve and display web pages.

- web server: An application that deliver (serves up) Web pages

## Test

1. Which layer is right below the Application layer?
a) Transport
b) Internetworking
c) Link Layer
d) Obtuse layer
2. What kind of document is used to describe widely used Application layer protocols?
86 CHAPTER 7. APPLICATION LAYER
a) DHCP
b) RFC
c) APPDOC
d) ISO 9000
3. Which of these is an idea that was invented in the Application layer?
a) 0f:2a:b3:1f:b3:1a
b) 192.168.3.14
c) www.khanacademy.com
d) http://www.dr-chuck.com/
4. Which of the following is not something that the Application
layer worries about?
a) Whether the client or server starts talking first
b) The format of the commands and responses exchanged
across a socket
c) How the window size changes as data is sent across a socket
d) How data is represented as it is sent across the network to
assure interoperability.
5. Which of these is an Application layer protocol?
a) HTTP
b) TCP
c) DHCP
d) Ethernet
6. What port would typically be used to talk to a web server?
a) 23
b) 80
c) 103
d) 143

7. What is the command that a web browser sends to a web
server to retrieve an web document?

a) RETR

b) DOCUMENT

c) 404

d) GET
8. What is the purpose of the “Content-type:” header when you
retrieve a document over the web protocol?
a) Tells the browser how to display the retrieved document
b) Tells the browser where to go if the document cannot be
found
c) Tells the browser whether or not the retrieved document is
empty
d) Tells the browser where the headers end and the content
starts
9. What common UNIX command can be used to send simple
commands to a web server?
a) ftp
b) ping
c) traceroute
d) telnet
10. What does an HTTP status code of “404” mean?
a) Document has moved
b) Successful document retrieval
c) Protocol error
d) Document not found
11. What characters are used to mark up HTML documents?
a) Less-than and greater-than signs < >
b) Exclamation points !
c) Square brackets [ ]
d) Curly brackets { }
12. What is a common application protocol for retrieving mail?
a) RFC
b) HTML
c) ICANN
d) IMAP
88 CHAPTER 7. APPLICATION LAYER
13. What application protocol does RFC15 describe?
a) telnet
b) ping
c) traceroute
d) www
14. What happens to a server application that is sending a large
file when the TCP layer has sent enough data to fill the window size and has not yet received an acknowledgement?
a) The application switches its transmission to a new socket
b) The application crashes and must be restarted
c) The application is paused until the remote computer
acknowledges that it has received some of the data
d) The closest gateway router starts to discard packets that
would exceed the window size
15. What is a “socket” on the Internet?
a) A way for devices to get wireless power
b) A way for devices to get an IP address
c) An entry in a routing table
d) A two-way data connection between a pair of client and
server applications
16. What must an application know to make a socket connection
in software?
a) The address of the server and the port number on the server
b) The route between the source and destination computers
c) Which part of the IP address is the network number
d) The initial size of the TCP window during transmission

# Chap 8

The Secure Transport Layer (SSL/TLS) was added to the existing four-layer model of the internet to provide security for data transmission. Public/private key encryption solved the key distribution problem of shared-secret encryption approaches, making it possible to routinely share public encryption keys across insecure media. By inserting the secure layer at the top of the Transport layer, the Application, Internetwork, and Link layers were not changed, making it easy to secure any Transport layer connection. Browsers support secure connections by changing the URL prefix from "http:" to "https:", and a series of trusted Certificate Authorities sign public keys to provide assurance of the organization's identity. The Secure Transport Layer provides a secure and easy-to-use mechanism for secure communications across the Internet at a massive scale.

## Vocab

- asymmetric key: An approach to encryption where one (public)
key is used to encrypt data prior to transmission and a different
(private) key is used to decrypt data once it is received.

- certificate authority: An organization that digitally signs public
keys after verifying that the name listed in the public key is actually the person or organization in possession of the public key.

- ciphertext: A scrambled version of a message that cannot be
read without knowing the decryption key and technique.

- decrypt: The act of transforming a ciphertext message to a plain
text message using a secret or key.

- encrypt: The act of transforming a plain text message to a ciphertext message using a secret or key.

- plain text: A readable message that is about to be encrypted
before being sent.

- private key: The portion of a key pair that is used to decrypt
transmissions.

- public key: The portion of a key pair that is used to encrypt
transmissions.

- shared secret: An approach to encryption that uses the same
key for encryption and decryption.

- SSL: Secure Sockets Layer. An approach that allows an application to request that a Transport layer connection is to be en-
crypted as it crosses the network. Similar to Transport Layer Security (TLS).

- TLS: Transport Layer Security. An approach that allows an application to request that a Transport layer connection is to be encrypted as it crosses the network. Similar to Secure Sockets Layer
(SSL).

## Test

1. How do we indicate that we want a secure connection when
using a web browser?

**a) Use https:// in the URL**

b) Use a secure web browser

c) Open an incognito window

d) Manually encode the address of the server using SHA1

2. Why is a shared-secret approach not suitable for use on the
Internet?

a) Because people would lose or misplace the secret

**b) It is difficult to distribute the secrets**

c) Encryption and decryption with shared secrets are too easily
broken

d) Encryption and decryption with shared secrets take too
much compute power

3. What is the underlying mathematical concept that makes
public/private key encryption secure?

a) Continuous functions

b) Taylor series

c) Karnaugh Maps

**d) Prime numbers**

4. Which of the keys can be sent across the Internet in plain
text without compromising security?

**a) Encryption key**

b) Decryption Key

c) Shared Secret

d) Univerally Safe Key (USK)

5. Where does the Secure Sockets Layer (SSL) fit in the fourlayer Internet architecture?

a) Below the Link layer

b) Between the Link and Internetworking layers

c) Between the Internetworking and Transport layers

**d) Between the Transport and Application layers**

6. If you were properly using https in a browser over WiFi in a
cafe, which of the following is the greatest risk to your losing
credit card information when making an online purchase?

a) Someone captured the packets that were sent across the
WiFi

b) Someone captured the packets in the gateway router

c) Someone captured the packets as they passed through a
core Intenet router

**d) You have a virus on your computer that is capturing
keystrokes**

7. With the Secure Sockets Layer, where are packets encrypted
and decrypted?

a) They are encrypted and decrypted as they pass through the

router
b) Each physical link has its own separate encryption

**c) They are encrypted in your computer and decrypted in the
server**

d) They are encrypted in the WiFi gateway and decrypted in the
last router before the destination computer

8. What changes to the IP layer were needed to make secure
socket layer (SSL) work?

**a) No changes were needed**

b) We had to add support for Secure IP (IPSEC)

c) We needed to support longer packets in IP

d) The Time-To-Live (TTL) value needed to be encrypted

9. If a rogue element was able to monitor all packets going
through an undersea cable and you were using public/private key encryption properly, which of the following
would be the most difficult for them to obtain?

a) What servers you were communicating with

b) How often you used the servers

c) How much data you retrieved from the servers

**d) Which documents you retrieved from the servers**

10. What is the purpose of a Certificate Authority in public/private key encryption?

a) To make sure people do not forge badges for learning activities

b) To make sure packets get routed to the correct destination
computer

**c) To assure us that a public key comes from the organization
it claims to be from**

d) To choose when a particular country must switch from IPv4
to IPv6

11. The ARPANET network was in operation starting in the 1960s.
Secure Sockets Layer (SSL) was not invented util the 1980s.
How did the ARPANET insure the security of the data on its
network?

a) By using public/private keys and encrypting all transmissions

b) By using encryption at the Link layer

**c) By making sure no one could access the physical links**

d) By only using secure WiFi routers

12. Which of these answers is “Security is fun” encrypted with a
Caesar Cipher shift of 1.

a) Ptsjduao rt dii

b) Wentudhs di dju

**c) Tfdvsjuz jt gvo**

d) Asdfghjk qw zxc

13. What Caesar Cipher shift was used to encrypt “V yvxr frphevgl”?

a) 1

b) 6

**c) 13**

d) 24
