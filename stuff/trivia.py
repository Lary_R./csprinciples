def check_ans(user, correct, rem, n):
    if user == correct:
        print ("Congrats, " + n + "!")
        return "correct"

    else:
        print ("Oops! Sorry, you didn't get that right. The correct answer was " + correct + ", and you chose " + user +". Try again!")
        return "no"




import random

questions = ["What is the most consumed drink in the world (except water)?","What was the first toy to be advertised on television?","How many US states are there?","In which country is the Taj Mahal?","Who was the US's second president?","Who is on the $100 bill in US currency?","How many letters are in this question","How many grandchildren did Queen Elizabth II have?","What sport is dubbed the 'king of sports'?","What is the capital of Europe?","What is the opposite of white?","Mary's mother had five children; Nana, Nene, Nini, and Nono. Who is the fifth child?","What is Microsoft's version of Google Docs?","Where was the Statue of Liberty made?","What is the last letter of the Greek alphabet?","What country invented ice cream?","What animal is on the Porche logo?","What is the correct spelling?","What is cynophobia?","How many holes are there in a t-shirt?","What does the first 'w' in a URL stand for?","What element is represented by the symbol Au?","Which planet is the biggest?","What shape are stop signs?","What does D&D stand for?"]

options = ["A) Soda \nB) Tea \nC) Juice \nD) Energy drinks","A) Mr. Potato Head \nB) Barbie \nC) Hot Wheels \nD) Tinker Toys","A) 15 \nB) 51 \nC) 50 \nD) 143","A) USA \nB) India \nC) Russia \nD) Egypt","A) John Quincy Adams \nB) Abraham Lincoln \nC) Donald Trump \nD) John Adams","A) George Washington \nB) Thomas Jefferson \nC) Benjamin Franklin \nD) Alexander Hamilton","A) 31 \nB) 32 \nC) 38 \nD) it's not a question","A) 4 \nB) 8 \nC) 6 \nC) 0","A) Soccer \nB) Football\nC) Golf\nD) Baseball","A) London\nB) Paris\nC) There is none\nD) Washington DC","A) Black\nB) Yellow\nC) Orange\nD) Red","A) Geoffrey\nB) Mary\nC) Nunu\nD) Alexis","A) Excel \nB) Word\nC) Onenote\nD) Powerpoint)","A) Argentina\nB) United States\nC) France\nD) United Kingdom","A) Sigma\nB) Omega\nC) Alpha\nD) PI","A) Germany\nB) England\nC) United States\nD) China","A) Horse\nB) Tiger\nC) Elephant\nD) Panther","A) Embaras\nB) Embarrass\nC) Embbarass\nD) Emmbaras","A) Fear of mice\nB) Fear of dogs\nC) Fear of squids\nD) Fear of sea animals","A) 0 (because it's new)\nB) As many as you can tear\nC) 4\nD) 12","A) Web\nB) World\nC) Wonder\nD) Wednesday","A) Silver\nB) Audi\nC) Gold\nD) Titanium","A) Earth\nB) Mars\nC) Saturn\nD) Venus","A) Hexagon\nB) Pentagon\nC) Pentagram\nD) Octagon","A) Dice and Doom\nB) Dice and Demons\nC) Dungeons and Dragons\nD) Doors and Devils"]

nums = ["0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24"]

answers = ["b","a","c","b","d","c","d","b","a","c","a","b","b","c","b","d","a","b","b","c","b","c","c","c","c"]

creds = "\nThis game was brought to you by the Tech students of Group A.\nOren ------- Ideas and Leadership\nKiersten --- Ideas and Code\nEimi ------- Typing and Code\nLary ------- Ideas, Food and editing \n\nGroup-A Gaming, copyright 2022"

name = input("\nHello! What should we call you? ")
ready = input("\nWelcome, " + name + "! This trivia game consists of multiple  questions, all of them are multiple choice! The game ends when you get 5 correct. Type a, b, c, or d in LOWERCASE to answer!\nPress enter to start.")

remaining = int(5)

random.shuffle(nums)

nmbr = 0

while remaining > 0:

    print("\nYou need to get "+str(remaining)+" more!\n")
    qnum = int(nums[nmbr])
    print(questions[qnum-1])
    uans = input(options[qnum-1]+"\n")
    #user answer ^^
    correct = check_ans(uans.lower(), answers[qnum-1],remaining, name)
    nmbr = nmbr+1
    if nmbr >= 25:
        nmbr = 0
    if correct == "correct":
        remaining = remaining - 1

print ("You won! You get an imaginary trophy on the winners' shelf! Come play again!")

print (creds)
