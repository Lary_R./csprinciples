# Group 1

A highly effective method for test review is to refer to your own notes on each big idea. This approach allows you to review the subject matter in a way that best suits your learning style, as you are the one who created the notes.

Another strategy for test preparation is to review past practice tests. This can provide insight into the phrasing of questions and the overall structure of the test, while also highlighting any areas where further review may be necessary. Additionally, you could attempt to recreate the test on your own and assess your performance, gaining valuable insight into your readiness for the actual exam.

Consulting your studyplan.md can also be advantageous, as it provides information on how the test is scored and offers online resources to assist with preparation, which can be revisited as needed.

To determine readiness for the multiple choice section of the APCSP, it is recommended to do practice questions. One resource for such practice questions is the CodeHS APCSP Exam review.

In terms of studying for the AP Computer Science Principles Diagnostic Test, there are several effective methods. These include reviewing past notes on big ideas such as Data.md, ComputingSystemsAndNetworks.md, and CreativeDevelopment.md, as well as looking over the study plan outlined in studyplan.md. Additionally, notes on classmates' repositories can be found on Jeff's website, and past big idea tests should be reviewed, including Big Idea 1: Creative Development, Big Idea 2: Data (with two tests available), and Big Idea 4: Computing Systems and Networks. Finally, it is important to review the full test taken at the beginning of the year and pay particular attention to the questions answered incorrectly on the reference sheet.

Numerous online resources are available to assist in preparing for the AP exams, including the College Board website, which offers tips for those taking the APCSP exam. Another valuable resource is Khan Academy, which has a dedicated APCSP course and an exam preparation course available at the end.
