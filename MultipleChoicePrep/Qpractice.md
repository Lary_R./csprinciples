# [AP Practice Exam](https://www.test-guide.com/ap-computer-science-principles-practice-exam.html)

## Question 1
You develop a program that lets you use different operations (addition, subtraction, multiplication, division, etc.) on a set of numbers derived from a database.

However, the actual test results give a random value, which leads you to suspect that there is a randomizing variable in your code. Which of the following should you do to make sure your code is correct?


- [ ] Rerunning the code

- [x] Code Tracing

- [ ] Using a code visualizer

- [ ] Using DISPLAY statements at different points of code

# Question 2

A school had a 90% pass rate for students that took the last AP exam. The school wants to use this achievement to advertise its services to families of prospective students.

Which of the following methods would be most effective in delivering this information to the families in a summarized manner?

- [ ] Email the prospective families that have middle-school-age children.

- [ ] Create a report based on the student body's overall performance for a marketing pamphlet.

- [x] Post an interactive pie chart about the topics and scores of the passing students on the schools' website

- [ ] Post the results of the passing students on social media sites.

# Question 3

A programmer is designing a code to analyze the number of people in middle age. The standard is set to be 42, and only citizens above this age are considered middle-aged citizens.

The algorithm can be designed with two different conditional statements, shown below. Will the two statements have the same operations?

I. IF (age > 42) 

II. IF (NOT (age < 42))

- [x] No

- [ ] Yes

- [ ] Only if all ages are 42

- [ ] Only if all ages are even numbers above 42

